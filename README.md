<div align = "center">

# :rocket: about me :rocket:

<!---![im'age](IMG_4900.jpg)--->
<img src="IMG20210922161401.jpg" width="300" height="350">

_Enjoy eating and sleeping_

</div>

<br><br>

- Name      : Ahmad Izzat Ikhwan Bin Muhamad
- Matric No : 196481
- Origin    : Hanguk-Saram

| <div align = "center"> Pros </div> | <div align = "center"> Cons </div> |
| :------: | :------: |
| Caring | Careless |
| Decisive | Pacifist |
| Savvy | Passive |
| Tactful | Sensitive |

<br><br>

## 

## Here some of my work!!!


| <div align = "center"> Task </div> | <div align = "center"> Hyperlink </div> |
| :------: | :------: |
| <div align='left'> Weekly Log </div> | [Link](https://gitlab.com/ratatouillle/weekly-log) |
| <div align='left'> Propulsion & Power Weekly Log </div> | [Link](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/tree/main) |
| <div align='left'> Resume </div> | [Link](https://www.canva.com/design/DAEzjYipaB0/TwmkiKJ1QczOXDJFp8XqeQ/view?utm_content=DAEzjYipaB0&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink) |
| <div align='left'> Report (Propulsion & Power Subsystem) </div> | [Link](https://1drv.ms/w/s!AjAMIeSjwyIViTQWcoiEPNQmrU6s?e=WTMdjL) |

